============
Installation
============

If you only want to use the package::

    $ activate myenv
    (myenv) $ conda install node_thermal_ref -c revesansparole
