Overview
========

.. {# pkglts, glabpkg

.. image:: https://ecosensornet.gitlab.io/node_thermal_ref/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://ecosensornet.gitlab.io/node_thermal_ref/

.. image:: https://ecosensornet.gitlab.io/node_thermal_ref/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/node_thermal_ref/0.0.1/

.. image:: https://ecosensornet.gitlab.io/node_thermal_ref/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/node_thermal_ref

.. image:: https://badge.fury.io/py/node_thermal_ref.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/node_thermal_ref


.. #}

Measure temp of thermal references
