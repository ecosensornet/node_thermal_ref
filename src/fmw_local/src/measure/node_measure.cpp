#include <ArduinoJson.h>
#include <sensor_max6675.h>

#include "../calibration/calibration.h"

#include "node_measure.h"

measure::Sample sample_cur;

MAX6675 th1 = MAX6675(12, 11, 9); // thermoCLK, thermoCS, thermoSO
MAX6675 th2 = MAX6675(7, 5, 3); // thermoCLK, thermoCS, thermoSO

void measure::setup() {
  th1.begin();
  th2.begin();
}


void measure::sample(){
    sample_cur.t1 = th1.read_celsius();
    sample_cur.t1_cal = calib.sensor1[0] + calib.sensor1[1] * sample_cur.t1;

    sample_cur.t2 = th2.read_celsius();
    sample_cur.t2_cal = calib.sensor2[0] + calib.sensor2[1] * sample_cur.t2;
}


void measure::fmt_config(String& msg) {
  StaticJsonDocument<128> doc;

  doc["t1"] = sample_cur.t1;
  doc["t1_cal"] = sample_cur.t1_cal;
  doc["t2"] = sample_cur.t2;
  doc["t2_cal"] = sample_cur.t2_cal;

  serializeJsonPretty(doc, msg);
}
