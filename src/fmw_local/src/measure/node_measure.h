#ifndef NODE_MEASURE_H
#define NODE_MEASURE_H

#include "../settings.h"

namespace measure {

typedef struct {
  float t1;
  float t1_cal;
  float t2;
  float t2_cal;
} Sample;

void setup();
void sample();

void fmt_config(String& msg);

}  // namespace measure

extern measure::Sample sample_cur;

#endif //  NODE_MEASURE_H
